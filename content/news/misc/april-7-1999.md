---
title:    "April 7, 1999"
date:     1999-04-07 00:00:00
category: misc
tags:     []
---
Updated the Mesa contributors section and added links to RPM Mesa
packages.
